<?php
use Cake\Routing\Router;

Router::plugin('RestApi', ['path' => '/rest-api'], function($routes){
    $routes->connect('/', ['controller' => 'Api', 'action' => 'index']);
    $routes->extensions(['json', 'xml']);
    $routes->fallbacks('DashedRoute');
});
