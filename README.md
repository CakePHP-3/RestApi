# RestApi plugin for CakePHP

## Installation

You can install this plugin into your CakePHP application using [composer](http://getcomposer.org).

The recommended way to install composer packages is:

```
composer require CakePHP-3/RestApi
```

# RestApi System Documentation

Valószinüleg aki ezt megtalálta tudja mi az a [REST Api](http://book.cakephp.org/3.0/en/development/rest.html) aki nem az nézze meg a linket.

## 1. Plugin engedélyezés a rendszer számára

``` php
./config/bootstrap.php
// Adjuk hozzá ezt a sort
Plugin::load('RestApi', ['bootstrap' => false, 'routes' => true, 'autoload' => true]);
```

## 2. Első lépések

Van egy alapértelmezett elérése a rest api-nak

http://domain.hu/rest-api/Api/web.json

Ez a rendszer annyiban különbözik a többitől hogy a path tagokat lecseréltem egy action paraméterre

GET request: http://domain.hu/rest-api/Api/web.json?action=test

POST request: http://domain.hu/rest-api/Api/web.json
POST params: {action=test}

PUT request: http://domain.hu/rest-api/Api/web.json
PUT params: {action=test}

DELETE request: http://domain.hu/rest-api/Api/web.json
DELETE params: {action=test}

Ezekat a rendszer úgy éri el a provideren belül, hogy elő körben a method-t és a action nevet összefűzi és CameCase-t csinál belőle

``` php 
public function getTest(){
    ...
}
public function postTest(){
    ...
}
public fucntion putTest(){
    ...
}
public function deleteTest(){
    ...
}
```

Továbbikaban csak GET-es megoldást fogom bemutatni de AZ ÖSSZESNÉL ugyan az lesz csak sok lenne mindent lemásolni

Most vegyünk egy nehezebb hívást:

A lényege az lesz hogy 2 kötelező paramétere lesz és egy ami nem lesz kötelező megadni

Kötelező paraméterek: [full_name, email]
Nem kötelező paraméterek: [age]

GET request: http://domain.hu/rest-api/Api/web.json?action=test&full_name=Bela&email=test@elek.hu -> Helyes lesz a hívás SUCCESS: TRUE

GET request: http://domain.hu/rest-api/Api/web.json?action=test&full_name=Bela&email=test@elek.hu&age=26 -> Helyes lesz a hívás SUCCESS: TRUE

GET request: http://domain.hu/rest-api/Api/web.json?action=test&full_name=Bela&age=26 -> Helytelen lesz a hívás SUCCESS: FALSE

GET request: http://domain.hu/rest-api/Api/web.json?action=test&email=test@elek.hu&age=26 -> Helytelen lesz a hívás SUCCESS: FALSE

És most a kód:

```php
public function getTest($full_name, $email, $age = 0) {
    return [
        'data' => compact('full_name', 'email', 'age')
    ];
}
```

Úgy lett megalkotva  rendszer hogy egy try catch részen fút keresztül ami azt teszi lehetővé hogy szinte soha nem tudunk olyan hibát kapni ami nem objectum lenne

Az alábbi példa müködése a következő, amikor meghívjuk a megfelelő method/action párost akkor egy Reflection osztály megnézni, hogy milyen sorrendbe és mik azok amik köteleő megadni és mik azok amik nem

Ebben az esetben tudni fogja, hogy van kettő kötelező és 1 ami nem, FONTOS nem kell figyelni a híváskor a sorrendre mert a Reflection osztálynak köszönhetően olyan sorrendben fogja function argumentum listáját meghívni ahogy egyezést talál rá.

Viszont egy dolog még nincs benne, ami a PHP-nak köszönhető hogy ha ugy adunk argumentum listának paramétert hogy vegyen van kötelező és nem kötelező paraméter azt maga a PHP fogja hibának dobni, ez még fejlesztés alatt van

Ha minden jól ment akkor egy objectot kapunk vissza
``` php
// TRUE esetén
{
    success: true,
    message: null,
    errorCode: 0,
    loggedIn: (boolean),
    data: {
        full_name: 'Bela',
        email: 'test@elek.hu',
        age: 26
    }
}

//FALSE esetén
{
    success: false,
    message: RestApiException álta vissza adott hiba szövege,
    errorCode: RestApiException által vissza adott hiba kód,
    loggedIn: (boolean),
    data: null
}
```

## 3. Tárolok (Provider-ek)

Az egész rest api ötlet onnan jött, hogy sok olyan alkalmazást kellet írnom ahol volt mobil api és web api is am sokszor megnehezitette a életemet

Na szóval a prviderek arra jók hogy szeparáltan tudjuk a megírt api function-t tárolni, és egyben el tudjuk szeparálni egymástól akát platform akár visibility szerint

Minden provider-nek kapnia kell egy use ApiProviderTrait; trait osztályt ami fel tudja majd dolgozni a később megírt apinkat

``` php
namespace ...;

class ApiProvider {
    use \RestApi\Provider\ApiProviderTrait;

    public function getTest($name, $email, $age = null, $gender = null){

    }

    private function getTest2($name, $email, $age = 0){
        return [
            'data' => compact('name', 'email', 'age')
        ];
    }
}
```

A trait osztály azért kell mert az dolgozza fel, hogy melyik tag fügvényt akarjuk használni