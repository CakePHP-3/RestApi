<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 02. 29.
 * Time: 21:51
 */

namespace RestApi\Controller;

use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Cake\Event\Event;

use RestApi\Controller\AppController as BaseController;
use RestApi\Controller\Component\RequestComponent;

use RestApi\Exception\RestApiEmptyValueException;
use RestApi\Exception\RestApiException;
use RestApi\Exception\RestApiInvalidCredentialsException;
use RestApi\Exception\RestApiMissingParamsException;
use RestApi\Exception\RestApiUnauthorizedException;
use RestApi\Exception\RestApiUnknownVisibility;

use RestApi\Utility\RestApiCallbacks;
use RestApi\Utility\RestRequest;
use RestApi\Utility\Utility;

/**
 * @property RequestComponent Request
 */
abstract class RestApiController extends BaseController implements RestApiCallbacks {
    private $authProvider = '\RestApi\Provider\AuthProvider';
    private $apiProvider = '\RestApi\Provider\ApiProvider';
    private $params = [];
    public static $userId = null;
    public $RestRequest = null;

    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->Auth->allow();
        $this->RestRequest = new RestRequest($this);
    }

    public function beforeFilter(Event $event) {
        return parent::beforeFilter($event);
    }

    /**
     * Itt tudunk megadni providert a RestApi-nak
     *
     * @param string $providerName - ApiProvider string-ként
     * @return $this
     */
    public function setApiProvider($providerName = null) {
        if (!empty($providerName)) {
            $this->apiProvider = $providerName;
        }
        return $this;
    }

    /**
     * Auth Provider megadása
     *
     * @param string $authProviderName - Auth Provider
     * @return $this
     */
    public function setAuthProvider($authProviderName = null) {
        if (!empty($authProviderName)) {
            $this->authProvider = $authProviderName;
        }
        return $this;
    }

    /**
     * @param $method - Alapértelmezetten 4 methodut van beégetve a rendszer amiket felül lehet értelmeztetni
     * @param callable $function - A function-ban van kifejtve hogy az adott method-al meghívott Api milyen halmazból vegye ki a forrás adatokat
     * @return $this
     */
    public function setRoutePrefix($method, callable $function) {
        $this->RestRequest->setMethod($method, $function);
        return $this;
    }

    /**
     * Ha nem a alapot használjuk akkor ezt is meg lehet hívni a lanc hívásnak a végén mert smoot :D
     */
    public function run() {
        if (empty($this->apiProvider)) {
            throw new RestApiException(
                'Not set ApiProvider class',
                RestApiException::ERROR_CODE_NOT_SET_API_PROVIDER
            );
        }
        $this->index();
    }

    public function index() {
        try {
            $this->params = $this->RestRequest->getParams();
            $functionName = $this->RestRequest->getFunctionName($this->apiProvider);
            $provider = new $this->apiProvider($functionName);
            $data = $provider->__getData();

            $response = NULL;
            switch ($data['visibility']) {
                case 'private':
                    $this->isApiAuthorized();
                    if (!empty($data['funcArgs']['required'])) {
                        Utility::checkMissingParams($data['funcArgs']['required'], $this->params);
                    }
                    $args = [];
                    if (!empty($data['funcArgs']['all'])) {
                        $args = Utility::sortArgs($data['funcArgs']['all'], $this->params, true);
                    }
                    $response = $provider->response($functionName, $args);
                    break;
                case 'public':
                    $response = $provider->response($functionName, $this->params);
                    break;
                default:
                    throw new RestApiUnknownVisibility();
                    break;
            }

            $returnType = gettype($response);
            if ($returnType === 'NULL') {
                throw new RestApiEmptyValueException();
            } else {
                $this->onResponse(true, $response);
            }
        } catch (RestApiException $e) {
            $this->onResponse(false, [
                'message' => $e->getMessage(),
                'errorCode' => $e->getCode()
            ]);
        }
    }

    /**
     * @throws RestApiEmptyValueException
     * @throws RestApiMissingParamsException
     * @throws RestApiUnauthorizedException
     */
    protected function isApiAuthorized() {
        if (!array_key_exists('token', $this->params)) {
            Utility::checkMissingParams(['token']);
            if (empty($this->params['token'])) {
                throw new RestApiEmptyValueException('token');
            }
        }

        if (!$this->__loginToToken($this->params['token']) && !(boolean)$this->Auth->user()) {
            throw new RestApiUnauthorizedException();
        }
    }

    /**
     * Callback funkció, a response lefutása elött hívodik meg
     *
     * @param array $response
     * @return bool
     */
    public function beforeResponse($response = array()) {
        return true;
    }

    /**
     * Callback funkció, a response lefutást követően hívodik meg
     *
     * @param array $response
     * @return bool
     */
    public function afterResponse($response = array()) {
        return true;
    }

    /**
     * CakePHP response metodusa json és xml formátumot adunk vissza
     *
     * @param null $success - Meg kell adni TRUE vagy FALSE értéket
     * @param array $response
     */
    public function onResponse($success = NULL, $response = []) {
        unset($response['success']);
        $default = array(
            'success' => $success,
            'message' => null,
            'errorCode' => 0,
            'loggedIn' => (boolean)$this->Auth->user(),
            'data' => null
        );
        $response = array_merge($default, $response);
        $response = array_merge($response, array(
            '_serialize' => array_keys($response)
        ));
        call_user_func([$this, 'beforeResponse'], [$response]);
        $this->set($response);
        call_user_func([$this, 'afterResponse'], [$response]);
        return;
    }

    /**
     * @param string $token - Token
     * @return bool|RestApiController::onResponse
     * @throws RestApiException
     */
    protected final function __loginToToken($token) {
        /**
         * @var UsersTable $Users
         */
        $authenticate = $this->Auth->config('authenticate');
        $model = $authenticate['Form']['userModel'];
        $Users = TableRegistry::get($model);
        if ($Users->hasField("token")) {
            if ($token && $Users->exists(["token" => $token])) {
                $conditions["token"] = $token;
                $UserData = $Users->find()->where($conditions)->limit(1)->toArray();
                if ($UserData) {
                    $this->Auth->setUser($UserData);
                    self::$userId = $this->Auth->user('id');
                    return true;
                }
            } else {
                $this->Auth->logout();
            }
        } else {
            throw new RestApiException(
                __d("rest_api", "No token field in %s model", [$model]),
                RestApiException::ERROR_CODE_NO_FILED_EXISTS
            );
        }
        return false;
    }

    /**
     * @return string - Token kulcs
     * @throws RestApiException
     */
    protected final function __getToken() {
        /**
         * @var UsersTable $UsersTable
         */
        $authenticate = $this->Auth->config('authenticate');
        $model = $authenticate['Form']['userModel'];
        $UsersTable = TableRegistry::get($model);
        if ($UsersTable->hasField("token")) {
            do {
                $token = Utility::makehash(60);
            } while ($UsersTable->exists(["token" => $token]));
            return $token;
        } else {
            throw new RestApiException(
                __d("rest_api", "No token field in User model"),
                RestApiException::ERROR_CODE_NO_FILED_EXISTS
            );
        }
    }

    protected function __loggedIn() {
        if (empty($this->params['token'])) {
            return false;
        }
        if ($this->__loginToToken($this->params['token']) && (boolean)$this->Auth->user()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Login de csak háttér folyamatra van
     *
     * @param $params
     * @param bool|false $return
     * @return array|bool|null
     * @throws RestApiInvalidCredentialsException
     * @throws RestApiMissingParamsException
     */
    protected function __login($params, $return = false) {
        /**
         * @var UsersTable $UsersTable
         */
        $authenticate = $this->Auth->config('authenticate');
        $form = $authenticate['Form']['fields'];
        $model = $authenticate["Form"]["userModel"];
        $UsersTable = TableRegistry::get($model);

        Utility::checkMissingParams($form);

        $fieldUsername = $form["username"];
        $fieldPassword = $form["password"];

        $loginData = [
            $fieldUsername => $params[$fieldUsername],
            $fieldPassword => $params[$fieldPassword]
        ];
        $login = $UsersTable->newEntity($loginData);
        if ($this->Auth->setUser($login->toArray())) {
            $data = $this->Auth->user();
            $token = $this->__getToken();

            $user = $UsersTable->get($data['id']);
            $user->token = $token;
            $UsersTable->save($user);

            if (!$return) {
                return $data;
            } else {
                return true;
            }
        } else {
            if (!$return) {
                throw new RestApiInvalidCredentialsException();
            } else {
                return false;
            }
        }
    }

    protected function __logout() {
        /**
         * @var UsersTable $UsersTable
         */
        $authenticate = $this->Auth->config('authenticate');
        $model = $authenticate["Form"]["userModel"];
        $UsersTable = TableRegistry::get($model);
        if ($this->__loginToToken($this->params['token']) && (boolean)$this->Auth->user()) {
            $userId = $this->Auth->user("id");
            $user = $UsersTable->get($userId);
            $user->token = NULL;
            $UsersTable->save($user);
            $this->Auth->logout();
            return true;
        }
        return true;
    }

}