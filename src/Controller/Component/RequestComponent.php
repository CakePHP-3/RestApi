<?php

/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 02. 29.
 * Time: 21:56
 */
namespace RestApi\Controller\Component;

use Cake\Controller\ComponentRegistry;
use Cake\Controller\Component;
use RestApi\Exception\RestApiException;

class RequestComponent extends Component {
    public $data = [];
    protected $actionName = 'action';
    protected $action = NULL;
    protected $method = NULL;

    public $getParams = [];
    public $postParams = [];
    public $putParams = [];
    public $deleteParams = [];

    protected $routePrefixes = array(
        'POST' => 'post',
        'GET' => 'get',
        'DELETE' => 'delete',
        'PUT' => 'put'
    );

    protected $methodData = [

    ];

    public function setMethodData($method, callable $callable){
        $method = strtoupper($method);
        $this->methodData[$method] = $callable();
    }

    public function loadAllParams() {
        $this->method();
        $this->loadGetParams();
        $this->loadPostParams();
        $this->loadPutParams();
        $this->loadDeleteParams();

        $this->data = array_merge(
            $this->getParams,
            $this->postParams,
            $this->putParams,
            $this->deleteParams
        );

        if (!array_key_exists($this->actionName, $this->data)) {
            throw new RestApiException(
                sprintf("Not fount '%s' param", $this->actionName),
                RestApiException::ERROR_CODE_UNDEFINED_ACTION
            );
        }else{
            $this->action = $this->data[$this->actionName];
        }

        return $this->data;
    }

    public static function allParams(){
        $registry = new ComponentRegistry();
        $request = new RequestComponent($registry);
        return $request->loadAllParams();
    }

    protected function method() {
        if(isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])){
            $this->method = strtoupper($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']);
        }else{
            $this->method = strtoupper($_SERVER['REQUEST_METHOD']);
        }

        if(!array_key_exists($this->method, $this->routePrefixes)){
            throw new RestApiException(
                'Not method request',
                RestApiException::ERROR_CODE_UNSUPPORTED_HTTP_METHOD
            );
        }
    }

    protected function loadGetParams() {
        $this->getParams = $this->request->query;
    }

    protected function loadPostParams() {
        $this->postParams = $this->request->query;
    }

    protected function loadPutParams() {
        parse_str(file_get_contents("php://input"), $this->putParams);
    }

    protected function loadDeleteParams() {
        parse_str(file_get_contents("php://input"), $this->deleteParams);
    }

    public function getFunctionName($class){
        $functionName = strtolower($this->method);
        $functionName.= ucfirst($this->action);

        if (method_exists($class, $functionName)) {
            return $functionName;
        }else{
            throw new RestApiException(
                sprintf('Not found function: %s', $functionName),
                RestApiException::ERROR_CODE_UNDEFINED_FUNCTION
            );
        }
    }
}