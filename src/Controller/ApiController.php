<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 01.
 * Time: 23:55
 */

namespace RestApi\Controller;

use Cake\Event\Event;

class ApiController extends RestApiController {
    public function initialize() {
        parent::initialize();
    }

    public function beforeFilter(Event $event) {
        return parent::beforeFilter($event);
    }

    public function web() {
        $this
            ->setAuthProvider('\RestApi\Provider\AuthProvider')
            ->setApiProvider('\RestApi\Provider\ApiProvider')
            ->setRoutePrefix('post', function () {
                return $this->request->data;
            })
            ->setRoutePrefix('get', function () {
                return $this->request->query;
            })
            ->setRoutePrefix('put', function () {
                $params = [];
                parse_str(file_get_contents("php://input"), $params);
                return $params;
            })
            ->setRoutePrefix('delete', function () {
                $params = [];
                parse_str(file_get_contents("php://input"), $params);
                return $params;
            })
            ->run();
    }
}