<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 10.
 * Time: 20:35
 */

namespace RestApi\Provider;

interface AuthProviderInterface {
    public function postLogin($username, $password);

    public function getLogout();

    public function getLoggedIn();
}

class AuthProvider implements AuthProviderInterface {
    public function postLogin($username, $password){

    }

    public function getLogout(){

    }

    public function getLoggedIn(){

    }
}