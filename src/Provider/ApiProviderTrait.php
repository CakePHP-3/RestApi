<?php
namespace RestApi\Provider;

use RestApi\Utility\ReflectionUtility;
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 20:17
 */
trait ApiProviderTrait {

    private $funcArgsFilter;
    private $funcArgsAll;
    private $visibility;

    public function __construct($functionName){
        $class = get_class();
        $this->funcArgsFilter = ReflectionUtility::getFuncArgNames($class, $functionName, true);
        $this->funcArgsAll = ReflectionUtility::getFuncArgNames($class, $functionName, false);
        $this->visibility = ReflectionUtility::getFunctionVisibility($class, $functionName);
    }

    public function __getData(){
        return [
            'funcArgs' => array_merge($this->funcArgsFilter, [
                'all' => $this->funcArgsAll
            ]),
            'visibility' => $this->visibility
        ];
    }

    public function response($functionName, $args = []){
        $class = get_class();
        return call_user_func_array(array($class, $functionName), $args);
    }
}