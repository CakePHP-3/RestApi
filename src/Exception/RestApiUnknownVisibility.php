<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:21
 */

namespace RestApi\Exception;


class RestApiUnknownVisibility extends RestApiException {
    public function __construct($message = 'Unknown Visibility', $code = self::ERROR_CODE_UNKNOWN_VISIBILITY, RestApiException $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}