<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:16
 */

namespace RestApi\Exception;


class RestApiValidateException extends RestApiException {
    public function __construct($filed = '', $message = "Not valid filed value: %s", $code = self::ERROR_CODE_VALIDATE_CODE, RestApiException $previous = null) {
        $message = sprintf($message, $filed);
        parent::__construct($message, $code, $previous);
    }
}