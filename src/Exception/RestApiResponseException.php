<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:20
 */

namespace RestApi\Exception;


class RestApiResponseException extends RestApiException {
    public $response = array();

    public function __construct($message = "", $response = array(), $code = 0, RestApiException $previous = null) {
        if (isset($response['success'])) {
            unset($response['success']);
        }
        $this->response = $response;
        parent::__construct($message, $code, $previous);
    }

    public function getResponse() {
        return $this->response;
    }
}