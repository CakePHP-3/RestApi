<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:15
 */

namespace RestApi\Exception;


class RestApiNoTemplateFileExistsException extends RestApiException {
    public function __construct($filename = '', $message = "No file exists: %s", $code = self::ERROR_CODE_NO_TEMPLATE_FILE_EXISTS, RestApiException $previous = null) {
        $message = sprintf($message, $filename);
        parent::__construct($message, $code, $previous);
    }
}