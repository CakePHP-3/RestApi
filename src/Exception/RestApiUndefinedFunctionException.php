<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:14
 */

namespace RestApi\Exception;

class RestApiUndefinedFunctionException extends RestApiException {
    public function __construct($function = '', $message = "Undefined function: %s", $code = self::ERROR_CODE_UNDEFINED_FUNCTION, RestApiException $previous = null) {
        $message = sprintf($message, $function);
        parent::__construct($message, $code, $previous);
    }
}