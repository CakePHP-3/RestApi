<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:15
 */

namespace RestApi\Exception;


class RestApiUnauthorizedException extends RestApiException {
    public function __construct($message = "No logged in", $code = self::ERROR_CODE_UNAUTHORIZED, RestApiException $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}