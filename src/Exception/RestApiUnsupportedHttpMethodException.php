<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:13
 */

namespace RestApi\Exception;

class RestApiUnsupportedHttpMethodException extends RestApiException {
    public function __construct($method = 'undefined', $message = "Unsupported HTTP method %s", $code = self::ERROR_CODE_UNSUPPORTED_HTTP_METHOD, RestApiException $previous = null) {
        $message = sprintf($message, $method);
        parent::__construct($message, $code, $previous);
    }
}