<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:16
 */

namespace RestApi\Exception;


class RestApiInvalidCredentialsException extends RestApiException {
    public function __construct($message = "Invalid email or password", $code = self::ERROR_CODE_INVALLID_CREDENTIALS, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}