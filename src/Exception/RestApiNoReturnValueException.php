<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:16
 */

namespace RestApi\Exception;

class RestApiNoReturnValueException extends RestApiException {
    public function __construct($message = "No return value", $code = self::ERROR_CODE_NO_RETURN_VALUE_CODE, RestApiException $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}