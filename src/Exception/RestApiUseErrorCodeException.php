<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:15
 */

namespace RestApi\Exception;


class RestApiUseErrorCodeException extends RestApiException {
    public function __construct($message = "Use error code", $code = self::ERROR_CODE_USE_ERROR_CODE, RestApiException $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}