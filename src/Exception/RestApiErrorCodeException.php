<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:21
 */

namespace RestApi\Exception;


class RestApiErrorCodeException extends RestApiException {

    public function __construct($code, $message = '', RestApiException $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    public function getResponse() {
        return array();
    }
}