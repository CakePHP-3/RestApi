<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:14
 */

namespace RestApi\Exception;


class RestApiMissingParamsException extends RestApiException {
    public $missingParams = array();

    // Redefine the exception so message isn't optional
    public function __construct($missingParams = array(), $message = "Missing params: %s", $code = self::ERROR_CODE_MISSING_PARAMS, RestApiException $previous = null) {
        $arrayToString = implode(", ", $missingParams);
        parent::__construct(sprintf($message, $arrayToString), $code, $previous);
    }
}