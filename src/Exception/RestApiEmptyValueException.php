<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:16
 */

namespace RestApi\Exception;


class RestApiEmptyValueException extends RestApiException {
    public function __construct($field = null, $message = "Empty value", $code = self::ERROR_CODE_EMPTY_VALUE, RestApiException $previous = null) {
        if ($field) {
            if (is_array($field)) {
                $field = implode(', ', $field);
            }
            $message = sprintf('%s: (%s)', $message, $field);
        }
        parent::__construct($message, $code, $previous);
    }
}