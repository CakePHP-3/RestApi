<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 02. 29.
 * Time: 22:04
 */

namespace RestApi\Exception;


use Exception;

class RestApiException extends Exception {
    /**
     * Nem támogaott Http Metodus
     */
    const ERROR_CODE_UNSUPPORTED_HTTP_METHOD = 1000;
    /**
     * Ismeretlen action paraméter
     */
    const ERROR_CODE_UNDEFINED_ACTION = 1001;
    /**
     * Ismeretlen fügvény
     */
    const ERROR_CODE_UNDEFINED_FUNCTION = 1002;
    /**
     * Hiányzó változók
     */
    const ERROR_CODE_MISSING_PARAMS = 1003;
    /**
     * Nem vagy bejeletkezve
     */
    const ERROR_CODE_UNAUTHORIZED = 1004;
    const ERROR_CODE_USE_ERROR_CODE = 1005;
    /**
     * Nem létező tmp fájl
     */
    const ERROR_CODE_NO_TEMPLATE_FILE_EXISTS = 1006;
    /**
     * Validálás hiba
     */
    const ERROR_CODE_VALIDATE_CODE = 1007;
    /**
     * Nincs visszatéritési érték a meghívott fügvénynek
     */
    const ERROR_CODE_NO_RETURN_VALUE_CODE = 1008;
    /**
     * Nincs értéke
     */
    const ERROR_CODE_EMPTY_VALUE = 1009;
    /**
     * Ha nincs filed
     */
    const ERROR_CODE_NO_FILED_EXISTS = 1010;
    /**
     * Nem jó authentikációs adatok
     */
    const ERROR_CODE_INVALLID_CREDENTIALS = 1011;

    /**
     * Nem támogatott láthatóság
     */
    const ERROR_CODE_UNKNOWN_VISIBILITY = 1012;

    /**
     * Nincs ApiProvider megdava
     */
    const ERROR_CODE_NOT_SET_API_PROVIDER = 1013;

    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
