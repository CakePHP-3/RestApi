<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 23:14
 */

namespace RestApi\Exception;

class RestApiUndefinedActionException extends RestApiException {
    public function __construct($action = "", $message = "Undefined action! You need to provide: '%s' parameter to determine which function to call!", $code = self::ERROR_CODE_UNDEFINED_ACTION, RestApiException $previous = null) {
        $message = sprintf($message, $action);
        parent::__construct($message, $code, $previous);
    }
}