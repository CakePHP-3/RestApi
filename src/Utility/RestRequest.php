<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 10.
 * Time: 22:04
 */

namespace RestApi\Utility;


use Cake\Controller\Controller;
use RestApi\Exception\RestApiException;

class RestRequest {
    private $controller;

    public $data = [];
    protected $actionName = 'action';
    protected $action = NULL;
    protected $method = NULL;

    public $getParams = [];
    public $postParams = [];
    public $putParams = [];
    public $deleteParams = [];

    protected $routePrefixes = array();

    public function __construct(Controller $controller) {
        $this->controller = $controller;
        $this->loadGetParams();
        $this->loadPostParams();
        $this->loadPutParams();
        $this->loadDeleteParams();
    }

    public function setMethod($method, callable $callable) {
        $method = strtoupper($method);
        $this->routePrefixes[$method] = $callable();
    }

    public function getParams() {
        $this->method();

        $this->data = $this->routePrefixes[$this->method];

        if (!array_key_exists($this->actionName, $this->data)) {
            throw new RestApiException(
                sprintf("Not fount '%s' param", $this->actionName),
                RestApiException::ERROR_CODE_UNDEFINED_ACTION
            );
        } else {
            $this->action = $this->data[$this->actionName];
        }

        return $this->data;
    }

    private function loadGetParams() {
        $this->setMethod('get', function () {
            return $this->controller->request->query;
        });
    }

    private function loadPostParams() {
        $this->setMethod('post', function () {
            return $this->controller->request->data;
        });
    }

    private function loadPutParams() {
        $this->setMethod('put', function () {
            $params = [];
            parse_str(file_get_contents("php://input"), $params);
            return $params;
        });
    }

    private function loadDeleteParams() {
        $this->setMethod('delete', function () {
            $params = [];
            parse_str(file_get_contents("php://input"), $params);
            return $params;
        });
    }

    protected function method() {
        if (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])) {
            $this->method = strtoupper($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']);
        } else {
            $this->method = strtoupper($_SERVER['REQUEST_METHOD']);
        }

        if (!array_key_exists($this->method, $this->routePrefixes)) {
            throw new RestApiException(
                'Not method request',
                RestApiException::ERROR_CODE_UNSUPPORTED_HTTP_METHOD
            );
        }
    }

    public function getFunctionName($class) {
        $functionName = strtolower($this->method);
        $functionName .= ucfirst($this->action);

        if (method_exists($class, $functionName)) {
            return $functionName;
        } else {
            throw new RestApiException(
                sprintf('Not found function: %s', $functionName),
                RestApiException::ERROR_CODE_UNDEFINED_FUNCTION
            );
        }
    }
}