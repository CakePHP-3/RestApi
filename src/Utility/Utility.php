<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 21:38
 */

namespace RestApi\Utility;


use RestApi\Controller\Component\RequestComponent;
use RestApi\Exception\RestApiMissingParamsException;

class Utility {
    /**
     * Random karakterek generálása
     * @param integer $n hash kód hossza
     * @param boolean $misc használjon speciális karaktereket is
     * @return string
     */
    public static function makehash($n = 16, $misc = false) {
        $x = '';
        for ($i = 1; $i <= $n; $i++) {
            $guess = mt_rand(1, 10);
            switch ($guess) {
                case 1:
                case 2:
                    $x.= chr(mt_rand(48, 57)); // number 20%
                    break;
                case 3:
                case 4:
                    if ($misc) {
                        $x.= chr(mt_rand(33, 46)); // misc 20%
                        break;
                    } else {
                        $x.= self::makehash(1, false);
                    }
                    break;
                case 5:
                case 6:
                    $x.= chr(mt_rand(65, 90)); // A-Z 20%
                    break;
                default:
                    $x.= chr(mt_rand(97, 122)); // a-z 40%
                    break;
            }
        }
        return $x;
    }

    /**
     * Megvizsgljuk hogy van e olyan paraméter amire szükségünk van
     *
     * @param array $requiredParams - A értékek amitket vizsgálni akarunk amik köteklezőek
     * @param array $requestParams - Ha nincs érték megadva akkor CakeRequest alap cuccokból veszi a dolgokat ha van akkor abból a többől key => value párosból
     * @param string $message - Az Exception-nak tudunk egyedi üzenetet adni
     * @throws RestApiMissingParamsException
     */
    public static function checkMissingParams(array $requiredParams, array $requestParams = [], $message = null) {
//        if (empty($requestParams)) {
//            $requiredParams = RequestComponent::allParams();
//            $keys = array_keys($requiredParams);
//        } else {
            $keys = array_keys($requestParams);
//        }


        $diff = array_diff($requiredParams, $keys);
        if (!empty($diff)) {
            if (!empty($message)) {
                throw new RestApiMissingParamsException($diff, $message);
            }
            throw new RestApiMissingParamsException($diff);
        }
    }


    public static function sortArgs(array $funcArgsAll, array $requestParams = [], $truncate = false) {
        $result = [];
        foreach($funcArgsAll as $arg) {
            if($truncate){
                if(isset($requestParams[$arg])){
                    $result[$arg] = $requestParams[$arg];
                }
            }else {
                $result[$arg] = $requestParams[$arg];
            }
        }
        return $result;
    }
}