<?php
namespace RestApi\Utility;
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 20:19
 */

interface RestApiCallbacks {
    public function beforeResponse($response = array());

    public function afterResponse($response = array());
}