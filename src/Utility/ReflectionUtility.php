<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2016. 03. 02.
 * Time: 0:00
 */

namespace RestApi\Utility;

use ReflectionClass;
use RestApi\Exception\RestApiException;

class ReflectionUtility {

    public static function getFunctionVisibility($_class, $funcName){
        $class = new ReflectionClass($_class);
        $method = $class->getMethod($funcName);
        if($method->isPrivate()){
            return 'private';
        }elseif($method->isProtected()){
            return 'protected';
        }elseif($method->isPublic()){
            return 'public';
        }else{
            throw new RestApiException('Unknown visibility', RestApiException::ERROR_CODE_UNKNOWN_VISIBILITY);
        }
    }

    public static function getFuncArgNames($_class, $funcName, $required = false) {
        $class = new ReflectionClass($_class);
        $method = $class->getMethod($funcName);
        $params = $method->getParameters();

        $result = array();
        $requiredFields = array();
        foreach ($params as $param) {
            $name = $param->getName();
            //debug($param->__toString());
            if($required) {
                if (false !== strpos($param->__toString(), '<required>')) {
                    $result[] = $name;
                    $requiredFields[] = $name;
                } else {
                    $result[] = $name;
                }
            }else{
                $result[] = $name;
            }
        }
        if($required){
            return array(
                'params' => $result,
                'required' => $requiredFields
            );
        }else {
            return $result;
        }
    }
}